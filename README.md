# Introduction to the DOM

**NOTE:** This lecture should be done live in front of the students, showing
them examples step by step up on the big screen. Make sure you are familiar with
the flow of the lecture and prepared to give real examples as you go. 

## Objectives

Students will learn about the Document Object Model and the role it plays in
rendering a site within the browser environment.


## SWBATS

+ HTML/DOM - Explain what the DOM is
+ HTML/DOM - Explain how the DOM came to be
+ HTML/DOM - Manipulate DOM elements


## Introduction

In this lesson, we will discuss the difference between HTML and something new:
the Document Object Model (DOM). HTML constructs all of the websites that we
visit. Let's examine how we can interact with the HTML of those sites.

#### Viewing the Source

From a Google Chrome browser, go to the URL of a webpage. For these examples, we
are using `https://www.wikipedia.org/`. To see the HTML of that page add on
`view-source:` to the front of the URL: `view-source:https://www.wikipedia.org/`

If you examine your browser, you will see the HTML used to construct Wikipedia.
It will look something like this:

![wikipedia-html](https://s3-us-west-2.amazonaws.com/curriculum-content/onboarding/introduction-to-dom/wikipedia-html.png)

The browser interprets the HTML you see along, with the styles (CSS) and
JavaScript, to construct the appearance in the browser. We call this process of
interpretation "rendering".

Notice that what we see in our view source tab looks very similar to the
structure of HTML you may have seen previously:

```html
<!DOCTYPE html>
<html>
  
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="style.css">
  </head>
  
  <body>
    <header>My Favorite Cat</header>
    <img id="kitten" class="" src="http://makeameme.org/media/templates/120/grumpy_cat.jpg" alt="" width="120" height="120">
  </body>
  <script src="https://raw.githubusercontent.com/learn-co-curriculum/js-and-the-web/master/spin.js" charset="utf-8"></script>

</html>

```

It may look like our browser is simply displaying the HTML that we see. For
example, surrounding the center image on the Wikipedia page appear the various
languages that Wikipedia has articles in. If we search through our HTML (`ctrl +
f` or `ctrl + f`) we can see that same title "Italiano" in the content of our
HTML. 

**NOTE:** A useful tool for displaying this (associating text on the rendered page with its HTML/DOM elements) is to use Chrome's element selector (`cmd + shift + c`). This is a magical tool. Make sure you are familiar with it first and use it often to reference rendered elements in the DOM.

So is it fair to say that our browser simply displays a webpage's HTML?

Not quite! Instead, our browser is directly displaying the Document Object Model
of the respective webpage. What does that mean? What is the Document Object
Model, and how is it different than the HTML for the page? 
### The Document Object Model

The best way to understand the Document Object Model is to see and interact with it. Let's play around!

#### 1. Open the Console

![opening-console](https://i.imgur.com/NVkBPSb.gif)

> From the webpage displaying Wikipedia, right click (or two fingers click on a Mac) and select the select the last option in the dropdown that you see, labeled "inspect". The Google Developer console will either pop up on the right or the bottom of your screen.

> Alternatively: look at the Chrome menu bar at the top of the page. Click on "View", then select "Developer", then "Developer Tools". This will open the Chrome Developer Console.

> Alternatively: `cmd + alt/option + i``

#### 2. Manipulate the DOM

![clicking-body](https://s3-us-west-2.amazonaws.com/curriculum-content/onboarding/introduction-to-dom/clicking-body.png)

When you open the Google Developer Console, you will see what looks like HTML.
There are head tags, body tags, divs, etc. Now, from inside the developer
console, click on the element that says `body`, and then press the delete button
on your keyboard. You should see the content of your Wikipedia page disappear.
You just deleted it!

![deleted-body](https://s3-us-west-2.amazonaws.com/curriculum-content/onboarding/introduction-to-dom/deleted-wiki.png)

 Now did you just delete the HTML? No. Let's prove it.

View the page source. Right click (or two fingers click on mac) on the lesson
page in the browser and select view page source. You will see the that the HTML
is just as it always was, with a body tag and lots of other elements inside.

![html-source](https://s3-us-west-2.amazonaws.com/curriculum-content/onboarding/introduction-to-dom/wikipedia-html.png)

The changes that the **developer console** caused, and the changes the
**developer console** currently displays are changes in the *Document Object
Model* (which we still haven't explained), but *not* in our *HTML*. Our webpage
now looks blank, reflecting the missing body in our DOM, even though our HTML
still has content in the body tags.

So what are we concluding? We're concluding that by changing the Document Object
Model, we can change the way our webpage displays. We can do this even if our
HTML is unchanged.

![mind blown](https://media1.giphy.com/media/xT0xeJpnrWC4XWblEk/giphy.gif)

### Tell it to me in bullet points

  * The Document Object Model (DOM) is a representation of the *current view* of the browser
  * The DOM can be manipulated without reloading a page
  * The HTML is the text in a file first used to display the page

So the HTML is essentially the starting point of the page's content. As we just
saw by deleting the body of the page, what is displayed can change. When we
change it, we change the Document Object Model, and that changes the appearance
in the browser. 

### This is still about JavaScript, right?

Ok, so what does this have to do with JavaScript?

Well, with JavaScript we can:
  - view a current representation of our Document Object Model
  - select specific portions of the DOM, and manipulate them, which changes what shows up in a browser window

Let' try it out:

#### Use JavaScript to view the current representation of the DOM

From inside the developer console, click on the "console" tab (you should see a
carrot and a blinking cursor).

There, type the word `document` and press Enter. You'll get a `#document`
returned. Click the Triangle and you'll see the DOM! If you followed along above
(when we deleted the `<body>` tag), you'll see a `<head>`, but no `<body>`,
which makes sense. We are using JavaScript to look at the **DOM**, and _not_ the
original HTML that the DOM was loaded from. 

#### Use JavaScript to manipulate the DOM

**NOTE: Refresh your browser to get the body content back.** 

Let's start over and remove the body tag **with JavaScript**. Open up the
console and type in the following:

```js
document.querySelector('body')
```

This will return something like this:

```html
<body class="srp tbo vasq">...</body>
```

Go ahead and click on that display triangle to see more.

It retrieves the body tag, which contains the web page. Ok, now let's do
something with this body. Open up the console, and type in the following:

```js
document.querySelector('body').remove()
```

Take a look at the top of the page again. The body is gone! We have updated the
DOM using JavaScript. Putting two and two together, we can see how this opens up
a lot of possibilities. Since we can already write programs in JavaScript, we
can now see how we can make sophisticated webpages, where we can (using logic in
JavaScript), delete/add/animate/update elements on the DOM. 

This is the core of every interesting (and uninteresting) interactive website
you have ever seen! To understand this foundation (browsers create the DOM,
JavaScript manipulates the DOM in real time) is all you need to be a web
developer. From here on out, everything is an abstraction built on these
concepts.

**QUERY THE STUDENTS:** 
  - How can I get the original DOM back? (refresh the page is the easiest way!)
  - Why does this reload the DOM? (The browser receives the base HTML + CSS and renders it anew!)

 When we access the DOM using `document.querySelector`, we're actually using
_JavaScript_.  This line can be added into JS scripts, allowing us to
programmatically alter content on the web page.  We're not limited to just
deleting the entire `body`, either! This interaction between JavaScript and the
DOM is how interactivity works on the internet. From buttons and forms, to
games and Google sheets, JavaScript is behind it all!

## Summary

We learned a lot in this section. Here's a recap:

* HTML is a markup language used to display content in a browser
* The browser generates something called the Document Object Model, which is an interactive representation of what a website is displaying
* When we change the appearance of a webpage, what we're really changing is the Document Object Model (not the base HTML it was made from), which directly determines the appearance displayed in the browser
* We can view and manipulate the Document Object Model by opening our developer tools
* We can also view our Document Object Model by opening the console and typing in the word `document`.
* We can select a specific piece of the DOM by using JavaScript, such as `document.querySelector('body')`, and we can also use JavaScript to alter our DOM with `document.querySelector('body').remove()`
